// variables2.rs
// Make me compile! Scroll down for hints :)

#[test]
fn main() {
    let x: i16 = 10;
    if x == 10 {
        println!("Ten!");
    } else {
        println!("Not ten!");
    }
}





























// The compiler message is saying that Rust cannot infer the type that the
// variable binding `x` has with what is given here.

// What happens if you annotate line 5 with a type annotation?
// There is still an error because x had no value

// What if you give x a value?
// It says there is 

// What if you do both?
// It compiles 

// What type should x be, anyway?
// i16

// What if x is the same type as 10? What if it's a different type?
// if its the same, then the program will run, if it's different, it won't run
